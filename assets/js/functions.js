document.addEventListener('DOMContentLoaded', function () {

    (function sliders() {
        let swiper = new Swiper('.js-index-slider', {
            slidesPerView: 1,
            spaceBetween: 30,
            loop: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            on: {
                init: function () {
                    $('#text-slide').html($('.swiper-slide-active').data('text'))
                },
                slideChangeTransitionEnd: function () {
                    $('#text-slide').html($('.swiper-slide-active').data('text'))
                },
            },
        });
    })();

    ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("map", {
            center: [51.872902, 39.450539],
            controls: [],
            zoom: 10
        });
        myPlacemarkWithContent = new ymaps.Placemark([52.036027, 39.385479], {}, {
            iconLayout: 'default#imageWithContent',
            iconImageHref: 'assets/img/map-pin.png',
            iconImageSize: [31, 44],
            iconImageOffset: [-24, -24],
            iconContentOffset: [15, 15],
        });
        myMap.behaviors.disable('scrollZoom');

        myMap.geoObjects
            .add(myPlacemarkWithContent);
    }


    function modifyText() {
        const menu = document.querySelector('.header-mobile');
        const body = document.querySelector('body');
        const menuLink = document.querySelectorAll('.header-mobile__link');
        const icon = document.querySelector('.header-inner__burger');
        const overlay = document.querySelector('.overlay');

        if (menu.classList.contains('is-active')) {
            menu.classList.remove('is-active');
            overlay.classList.remove('is-active');
            body.style.overflow = 'auto';
        } else {
            menu.classList.add('is-active');
            overlay.classList.add('is-active');
            body.style.overflow = 'hidden';
        }

        for (i = 0; i < menuLink.length; i++) {
            menuLink[i].addEventListener('click', function () {
                menu.classList.remove('is-active');
                overlay.classList.remove('is-active');
                body.style.overflow = 'auto';
                icon.classList.remove('active');
            });
        }
    }

    (function () {
        const icon = document.querySelector('.header-inner__burger');
        icon.addEventListener('click',modifyText,false)
    })();

    (function ieImageFix() {
        let userAgent = window.navigator.userAgent,
            ieReg = /msie|Trident.*rv[ :]*11\./gi,
            ie = ieReg.test(userAgent);

        if(ie) {
            $('.ie-image-container').each(function () {
                let $container = $(this),
                    imgUrl = $container.find('img').prop('src');

                if (imgUrl) {
                    $container.css('backgroundImage', 'url(' + imgUrl + ')').addClass('ie-fix-object-fit');
                }
            });
        }
    })();


    $('.js-slider-auto-selekt').each(function (index, element) {

        let galleryTop = new Swiper($(element).find('.gallery-top'), {
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            loop: true,
            loopedSlides: 20
        });

        let galleryThumbs = new Swiper($(element).find('.gallery-thumbs'), {
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true,
            loop: true,
            loopedSlides: 20
        });

        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;
    });


    (function (){
        let menuLink = $('.js-menu');

        menuLink.hover(
            function() {
                $('.menu').addClass('menu-is-active');
            }
        );

        menuLink.mouseout(
            function() {
                $('.menu').removeClass('menu-is-active');
            }
        );
    })();


    $('.select').niceSelect();

});

//# sourceMappingURL=main.js.map